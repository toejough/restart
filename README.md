# Restart

command restart utility for shell pipelines

## What it does

1. Starts a command with args.
1. Listens for a line to come in on stdin.
1. If the command isn't done when a line comes in, the command is killed.
1. Restarts the command.
1. Repeat.

## How to use it

`<trigger command> | restart command with args`

## Example

* Start `go test`
* Watch the current directory for changes with `fswatch.`.  
* Restart `go test` whenever a file change is detected, stopping an existing run, if any.

`fswatch . | restart go test`

## Disclaimer

This utility was written to scratch an itch, and for the moment, it does.  I'm
not sure when or if I'll have the occassion or need to improve it, but I'd love
to hear if you find it useful and would like more features.