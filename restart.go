package main

import (
	"bufio"
	"io"
	"os"
	"os/exec"
	"syscall"
	"time"

	"github.com/fatih/color"
)

func main() {
	// syscall stuff copied from https://medium.com/@felixge/killing-a-child-process-and-all-of-its-children-in-go-54079af94773
	update := color.New(color.FgCyan).FprintfFunc()
	alert := color.New(color.FgRed).FprintfFunc()
	progress := color.New(color.FgYellow).FprintfFunc()
	success := color.New(color.FgGreen).FprintfFunc()

	args := os.Args[1:]
	success(os.Stderr, "Initial launch.  Starting new process...\n")
	command := exec.Command(args[0], args[1:]...)
	command.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	command.Start()
	reader := bufio.NewReader(os.Stdin)
	// XXX completion message
	for {
		_, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			alert(os.Stderr, "unable to continue: %v\n", err)
			os.Exit(1)
		}
		update(os.Stderr, "New input received.  Checking prior process...\n")
		done := make(chan bool)
		go func() {
			command.Wait()
			done <- true
			close(done)
		}()
		select {
		case <-done:
			break
		case <-time.After(100 * time.Millisecond):
			alert(os.Stderr, "Prior process hasn't exited.  Killing...\n")
			syscall.Kill(-command.Process.Pid, syscall.SIGKILL)
			progress(os.Stderr, "Waiting...\n")
			command.Wait()
		}
		success(os.Stderr, "Prior process has exited.  Starting new process...\n")
		command = exec.Command(args[0], args[1:]...)
		command.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
		command.Stdout = os.Stdout
		command.Stderr = os.Stderr
		command.Start()
	}
	command.Wait()
	os.Exit(command.ProcessState.ExitCode())
}
